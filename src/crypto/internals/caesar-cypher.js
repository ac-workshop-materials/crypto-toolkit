const { getCharset } = require("./chars");

const deriveNewPosition = (length, index, distance) => {
  const newIndex = index + distance;

  if (newIndex < 0) {
    return length - Math.abs(newIndex);
  }

  return newIndex >= length ? Math.abs(newIndex - length) : newIndex;
};

const deriveNewChar = (charset, char, distance) =>
  charset[deriveNewPosition(charset.length, charset.indexOf(char), distance)];

const rotate = (char, distance) =>
  deriveNewChar(getCharset(char), char, distance);

//TODO: Support rotations distances greater than 12
const caesar = (pass, distance) =>
  pass
    .split("")
    .map((char) => rotate(char, distance))
    .join("");

module.exports = caesar;
