const chars = {
  NUMBERS: "0123456789",
  LOWERCASE: "abcdefghijklmnopqrstuvwxyz",
  UPPERCASE: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
  SPECIAL: "!@#$%^&*(){}[]=<>/,.|~?",
};

const isNumber = (char) => !isNaN(parseInt(char, 10));

const getCharType = (char) => {
  if (chars.SPECIAL.includes(char)) return "SPECIAL";

  if (isNumber(char)) return "NUMBERS";

  return char === char.toLowerCase() ? "LOWERCASE" : "UPPERCASE";
};

const getCharset = (char) => chars[getCharType(char)];

const getRandomChar = () => {
  const allChars = Object.values(chars).join("");
  return allChars[Math.floor(Math.random() * allChars.length)];
};

module.exports = { getRandomChar, getCharset };
