const caesar = require('./internals/caesar-cypher');

/**
 * simple crypto to encode things we want to save as plain text
 * refer to ./internals/chars.js for supported chars
 * 
 * should be enough to keep cadets away cadets
 * */

const encrypt = pass => caesar(pass, 7);

const decrypt = pass => caesar(pass, -7);

module.exports = { encrypt, decrypt };

