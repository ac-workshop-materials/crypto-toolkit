const { getRandomChar } = require("./internals/chars");

module.exports = passwordGenerator = (length) => {
  let password = "";
  for (let i = 0; i < length; i++) {
    password += getRandomChar();
  }
  return password;
};
