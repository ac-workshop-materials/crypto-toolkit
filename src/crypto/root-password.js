const passwordGenerator = require("./password-generator");
const rot7 = require("../crypto/rot7");

const getRootPassword = () => {
  if (!process.env.secret) {
    process.env.secret = rot7.encrypt(passwordGenerator(10));
  }

  return rot7.decrypt(process.env.secret);
};

module.exports = getRootPassword;
