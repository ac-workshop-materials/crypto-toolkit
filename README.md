# Toolkit for <A/C> workshop exercises

This aims to be a zero-dependency utility package containing useful tools to be used across <A/C>'s
Workshop libraries.

## Available

- Password Generator -> Generates a `n` length password. Example: `c~Y@EKefp`
- Rot13 -> A bootleg Rot13 encrypter/decrypter
- Rot7 -> Like rot13 but works with numbers and special chars aswell

