module.exports = {
    rot7: require('./src/crypto/rot7'),
    rot13: require('./src/crypto/rot13'),
    passwordGenerator: require('./src/crypto/password-generator'),
    getRootPassword: require('./src/crypto/root-password')
}


